#ifndef HTTPHOOKMAINVIEW_H
#define HTTPHOOKMAINVIEW_H

#include <QWidget>
#include "HttpServer.h"
#include "HttpHook.h"

namespace Ui {
class HttpHookMainView;
}

class HttpHookMainView : public QWidget
{
    Q_OBJECT

public:
    explicit HttpHookMainView(QWidget *parent = nullptr);
    ~HttpHookMainView();

private slots:
    void on_start_stop_clicked();
    void on_save_clicked();
    void AddTabSlot();
    void AboutSlot();

private:
    Ui::HttpHookMainView *ui;
    HttpServer* server;

    std::vector<HttpHook*> hooks;

    void Start();
    void Stop();
    void AutoLoad();
    void UpdateStatus();
    HttpHook* AddTab();

    bool running = false;
};

#endif // HTTPHOOKMAINVIEW_H
