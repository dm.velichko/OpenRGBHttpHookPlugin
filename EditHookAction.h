#ifndef EDITHOOKACTION_H
#define EDITHOOKACTION_H

#include <QWidget>

namespace Ui {
class EditHookAction;
}

class EditHookAction : public QWidget
{
    Q_OBJECT

public:
    explicit EditHookAction(QWidget *parent = nullptr);
    ~EditHookAction();

    void Reload();
signals:
    void updated(int, std::string);

private slots:
     void on_action_currentIndexChanged(int);
     void on_sub_action_currentIndexChanged(int);

private:
    Ui::EditHookAction *ui;
};

#endif // EDITHOOKACTION_H
