#ifndef HOOKSETTINGS_H
#define HOOKSETTINGS_H

#include "json.hpp"
#include "filesystem.h"

using json = nlohmann::json;

class HookSettings
{
public:
    static void Save(json);
    static json Load();

private:
    static bool CreateSettingsDirectory();
    static filesystem::path SettingsFolder();
};

#endif // HOOKSETTINGS_H
