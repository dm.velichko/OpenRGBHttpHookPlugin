#ifndef HOOKACTIONS_H
#define HOOKACTIONS_H

#include <QStringList>

enum
{
    HOOK_ACTION_LOAD_PROFILE    = 0,
    HOOK_ACTION_TURN_OFF        = 1,
    HOOK_ACTION_EFFECT_PLUGIN   = 2
};

static QStringList HOOK_ACTIONS = QStringList({"Load profile", "Turn OFF", "Effects plugin action"});

#endif // HOOKACTIONS_H
