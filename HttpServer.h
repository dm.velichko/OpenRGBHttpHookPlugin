#ifndef HTTPSERVER_H
#define HTTPSERVER_H

#include "httplib.h"

class HttpServer
{
public:
    HttpServer();

    void SetHookCallback(std::function<bool(std::string, std::string)> callback);
    void SetupRoute();

    void Start(std::string ip, int port);
    void Stop();

    bool State();

private:
    httplib::Server svr;

    std::thread* t = nullptr;
    void ThreadFunction();

    std::function<bool(std::string, std::string)> callback;

    std::string ip = "0.0.0.0";
    int port = 8080;
};

#endif // HTTPSERVER_H
